---
title: HTCondor overview
---

# HTCondor

**Ref:**
<https://htcondor.readthedocs.io/en/latest/users-manual/welcome-to-htcondor.html>

## What is HTCondor?

[HTCondor](https://htcondor.readthedocs.io/) is (according to its own
documentation):

> a software system that creates a High-Throughput Computing (HTC) environment.

In reality, HTCondor is a system for describing a workflow containing one or
more jobs and their relationships, and then executing that workflow on one
or more machines.

## What is a workflow?

A workflow is the task or tasks that you want to complete.
This can be as simple as a single command that does one thing, or as
complicated as a full data-analysis pipeline that spans hundreds of
thousands of jobs in many stages with complicated interdependencies.
