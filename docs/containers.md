---
title: Containers
---

# Using containers with HTCondor

**Ref:**
<https://htcondor.readthedocs.io/en/latest/admin-manual/singularity-support.html>

Containers are an extremely convenient way of pre-packaging an
environment with all necessary software, rather than relying on
the remote execute machine to have what you need.
HTCondor uses [Singularity](https://sylabs.io/singularity/) to
execute jobs inside containers, but can execute directly from
docker images if given.

## Specifying the image to run

There are two extra commands you need to include in your job
submit file to use singularity.

1. Ensure that the target machine has singularity:

    ```ini
    requirements = HasSingularity
    ```

2. Specify the image to run, e.g.:

    ```ini
    +SingularityImage = "docker://thing:tag"
    ```

For example, if consider the following Python script that uses the
[walrus operator](https://docs.python.org/3/whatsnew/3.8.html#assignment-expressions)
introduced in Python 3.8:

!!! example "`council.py`"
    ```python
    #!/usr/bin/env python3
    import re
    import sys
    council_regex = re.compile(r"\A[NY],Y,(\w+),(\w+),")
    for line in sys.stdin:
        if match := council_regex.match(line):
            print("{0[1]} {0[0]} is a council member".format(match.groups()))
    ```

If we want to run this over our roster to extract the council members, we would
need to ensure that Python >= 3.8 is available for our job, so we do that by
using a container.
We can construct our submit file like this

!!! example "`container-job.sub`"
    ```ini
    executable = /usr/local/bin/python3
    input = roster.txt
    output = council-members.txt
    error = container.err
    log = container.log
    arguments = council.py

    requirements = HasSingularity
    +SingularityImage = "docker://python:latest"

    should_transfer_files = YES
    transfer_executable = False
    transfer_input_files = council.py

    queue
    ```

## Using CVMFS singularity containers

The above container specification

```ini
+SingularityImage = "docker://python:latest"
```

instructs HTCondor to download the right container from hub.docker.com
on-the-fly for each job.
This requires each execute node to have access to the internet (not
standard), but also doesn't scale well when the container being downloaded
is very large.

The Open Science Grid operates a system to pre-convert (from Docker to
Singularity) and distribute containers using CVMFS, which already includes
a number of IGWN analysis containers.
This adds a requirement that the target machine has the
`singularity.opensciencegrid.org` CVMFS repository available.
The (OSG-custom) `HAS_SINGULARITY` requirement can be used to ensure
that everything is configured to use containers from that repo.

So, we can modify the above submit file to use the CVMFS path of the same
image:

```ini
requirements = HAS_SINGULARITY =?= TRUE
+SingularityImage = "/cvmfs/singularity.opensciencegrid.org/library/python:latest"
```

## Local image files

HTCondor can also transfer the image as if it were an input file; for that
the submit file might include

```ini
...
requirements = HasSingularity
+SingularityImage = "image.sif"
should_transfer_files = YES
transfer_input_files = image.sif
...
```
