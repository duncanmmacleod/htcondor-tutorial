# Jobs in HTCondor

**Ref:**
<https://htcondor.readthedocs.io/en/latest/users-manual/running-a-job-steps.html>

## What is a job?

A job is a single task to be executed on a single resource.
This is the smallest unit of work in an HTCondor workflow;
it is described using a submit file.

For example, consider this simple command:

```bash
/bin/sh --version
```

It includes an `executable` (`/bin/sh`) and (optionally)
some `arguments` (`--version`) and produces some `output`.
To describe this to HTCondor we need to write a file that
includes each of those components and will look something like this:

!!! example "job.sub"
    ```ini
    executable = /bin/sh
    arguments = --version
    output = shell-version.out
    error = shell-version.err

    queue
    ```

The `output` and `error` commands give file paths in which HTCondor
should record the `stdout` and `stderr` streams respectively.

The `queue` statement here is what tells HTCondor that you are
finished describing the job, and to add it to the queue for processing.
This should be the final line in your submit file.

## Submitting a job

Jobs are added to the queue using the `condor_submit` executable.
If the above job description is written to the file `job.sub`, then the
syntax to submit that for processing is trivial:

```shell
condor_submit job.sub
```

The output should look something like this:

```console
$ condor_submit job.sub
Submitting job(s).
1 job(s) submitted to cluster 22139468.
```

## The basic life cycle of a job

After submission to the queue, jobs typically follow a standard track
through the system:

1. The job is added to the queue by the scheduler (`I`)
2. When its turn arrives, the job is matched against available resources,
   sent to that resource, and started (`R`)
3. Once a job finishes, the job is removed from the queue (`C`)

The parenthesis after each point here is the 'status' of the job as
reported by HTCondor, respectively ('idle', 'running', and 'complete').

!!! note "Complete != Passed"
    In general HTCondor doesn't really care if you job passed
    or failed, and just reports 'complete'.
    You can dig in to the details to get the `ExitCode` of a job,
    where `0` normally denotes success, and everything else a failure.

## Tracking a job

Jobs in the queue can be tracked using the `condor_q` script, for esample:

```console
$ condor_q


-- Schedd: cl8.supercomputingwales.ac.uk : <172.2.1.88:9618?... @ 04/19/21 09:09:59
OWNER          BATCH_NAME      SUBMITTED   DONE   RUN    IDLE  TOTAL JOB_IDS
duncan.macleod ID: 22139468   4/19 09:09      _      _      1      1 22139468.0

Total for query: 1 jobs; 0 completed, 0 removed, 1 idle, 0 running, 0 held, 0 suspended
Total for duncan.macleod: 1 jobs; 0 completed, 0 removed, 1 idle, 0 running, 0 held, 0 suspended
Total for all users: 23254 jobs; 0 completed, 0 removed, 18116 idle, 4613 running, 525 held, 0 suspended

```

Here we see my job in the queue in the `IDLE` state, meaning it hasn't
been considered yet.
Eventually it will move in the `RUN` state, and finally to the `DONE` state.

!!! note "Single jobs don't linger in `DONE`"

    Individual jobs are typically removed from the queue very quickly
    after they complete, so it is unusual to see anything in the `DONE`
    column for a single job.
    That becomes a useful column when dealing with DAGs (more later).

## Held jobs

### Why do jobs get held?

When something goes wrong with a job, HTCondor will attempt to remedy that
by taking one of the following actions:

- stop the job and remove it from the queue (job exited, job killed, ...)
- stop the job and ask the scheduler to match it again with (in general) a
  new machine (that host has been reserved by a higher-priority job/user,
  that host is not responding, ...)

However, there are cirumstances when HTCondor doesn't have a clear action to
take, in which case it will place the job in the `HOLD` (`H`) state.

### Determining the hold reason {: #holdreason }

If a job is put on hold, HTCondor will set (or update) the `HoldReason` to
a human-readable string that describes (or attempts to) the underlying issue.

Consider the following job:

!!! example "`json.sub`"
    ```
    executable = /usr/bin/python3
    input = data.json
    arguments = -m json.tool
    queue
    ```

If the `data.json` file isn't found in the submission directory,
you should see the following:

```console
$ condor_q $USER


-- Schedd: cl8.supercomputingwales.ac.uk : <172.2.1.88:9618?... @ 04/20/21 06:34:05
OWNER          BATCH_NAME      SUBMITTED   DONE   RUN    IDLE   HOLD  TOTAL JOB_IDS
duncan.macleod ID: 22143521   4/20 06:34      _      _      _      1      1 22143521.0

Total for query: 1 jobs; 0 completed, 0 removed, 0 idle, 0 running, 1 held, 0 suspended
Total for all users: 1139 jobs; 0 completed, 0 removed, 0 idle, 614 running, 525 held, 0 suspended

```

indicating that the job is now on hold.
Passing the `-held` flag to `condor_q` prints the hold reason:

```console
$ condor_q $USER -held
...
22143521.0   duncan.macleod  4/20 06:34 Error from slot1_6@ccr0001.hawk.supercomputingwales.ac.uk: Failed to open /home/user/work/data.json' as standard input: No such file or directory (errno 2)
...
```

### Releasing a job {: #release }

If the hold reason is easy to fix (find the missing file and put it in place),
you can tell HTCondor to try the job again by using the `condor_release` command:

```console
$ condor_release 22143521.0
Job 22143521.0 released
```

## Diagnosing a job

Once the job is complete, it is removed from the queue, so `condor_q`
won't tell you anything about it.

### 1. Check the output file {: #check-output }

Typically you can infer the job status from its output.
In the above example, after the job is complete I should have an output
file `shell-version.out` written into the directory from which I
submitted the job.
This file should look like this:

!!! example "shell-version.out"
    ```text
    GNU bash, version 4.2.46(2)-release (x86_64-redhat-linux-gnu)
    Copyright (C) 2011 Free Software Foundation, Inc.
    License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>

    This is free software; you are free to change and redistribute it.
    There is NO WARRANTY, to the extent permitted by law.
    ```

If that file doesn't exit, or is empty, I presume my job failed.

### 2. Check the `error` file {: #check-error }

If the `output` file indicates something is wrong, check the `error` file,
most standard script failures will dump useful information there.

### 3. Ask `condor_history` {: #check-condor_history }

If neither the `output` or the `error` files give you the information you
need, you may need to dive into the HTCondor history.

HTCondor does keep a record of past jobs which can be queried using
the `condor_history` executable.
The easiest syntax is to pass it the __ID__ of the job:

```console
$ condor_history 22139468
 ID     OWNER          SUBMITTED   RUN_TIME     ST COMPLETED   CMD
22139468.0   duncan.macleod  4/19 09:02   0+00:00:01 C   4/19 09:03 /bin/sh --version
```

!!! tip "Use `-match 1` with `condor_history`"
    By default, `condor_history` searches through its history of all
    jobs to find all matches to the ID it was given.
    You can pass `-match 1` to tell it to stop searching as soon as
    a single match is found - this will greatly speed up history queries.

This doesn't tell me very much.
We need to understand [ClassAds](classads.md) to dive any deeper, more later.

## Other job options {: #options }

This section describes other useful job options that can be used in a
submit file.

For a more complete listing, please see
<https://htcondor.readthedocs.io/en/latest/man-pages/condor_submit.html#submit-description-file-commands>.

### `accounting_group`

All computing usage in IGWN should be accounted for, allowing us to
understand our software and its inefficiencies, and to budget for
resource purchases in the future.
As a result, when submitted to IGWN HTCondor pools, it is mandatory
to include the `accounting_group` option in all submit files.
For example:

```ini
accounting_group = ligo.dev.o4.cbc.pe.pesummary
```

For full details, and for a tool to determine which tag to use, please
see <https://accounting.ligo.org/user>.

### `accounting_group_user`

When submitting to IGWN HTCondor pools, if the username of the account
that submits the jobs is not registered as a valid accounting user,
the `accounting_group_user` option is required to allow usage to
be accounted back to a real person, e.g.

```ini
accounting_group_user = marie.curie
```

This is most-commonly required when submitting from a shared group
account (e.g. `cbc`).

### `preserve_relative_paths`

See [_Preserving relative paths_](file-transfer.md#preserve_relative_paths).

### `request_cpus`

**Default value**: `<site-specific>`

By default each job is run on a single core on one machine.
To request multiple cores on a single machine, specify
`request_cpus` with a higher number, e.g.

```ini
request_cpus = 4
```

!!! tip "Use `condor_config_val` to check the default"
    You can use the `condor_config_Val` executable to check
    the default via:

    ```console
    $ condor_config_val JOB_DEFAULT_REQUESTCPUS
    1
    ```

### `request_disk`

**Default value**: `<site-specific>`  
**Default unit**: KB

If your job requires a large slot of disk space on the remote machine
on which to write data (before transferring back to the submit machine),
you should specify that with `request_disk`.

```ini
request_disk = 10240
```

!!! tip "Can use human-readable metric units"
    This value can be given as a human-readable metric value with a unit.
    `K` or `KB` (kilobytes), `M` or `MB` (megabytes),
    `G` or `GB` (gigabytes), or `T` or `TB` (terabytes), e.g.

    ```ini
    request_disk = 128GB
    ```

!!! tip "Use `condor_config_val` to check the default"
    You can use the `condor_config_Val` executable to check
    the default via:

    ```console
    $ condor_config_val JOB_DEFAULT_REQUESTDISK
    DiskUsage
    ```

    (here the value `DiskUsage` just indicates that whatever the job uses,
    it gets)

### `request_memory`

**Default value**: `<site-specific>`  
**Default unit**: MB

HTCondor tracks how much memory a job uses;
jobs with non-trivial memory requirements should specify how much RAM their
job will need, e.g:

```ini
request_memory = 16384
```

!!! tip "Can use human-readable metric units"
    This value can be given as a human-readable metric value with a unit.
    `K` or `KB` (kilobytes), `M` or `MB` (megabytes),
    `G` or `GB` (gigabytes), or `T` or `TB` (terabytes), e.g.

    ```ini
    request_memory = 16GB
    ```

!!! tip "Find the sweet spot between too large and too small"
    Some administrators will configure their pools to evict jobs that
    exceed the stated requirement, so it should be set no too small,
    but also setting this number too large will result in HTCondor being
    unable to match any resources that actually provide that much RAM.

!!! tip "Use `condor_config_val` to check the default"
    You can use the `condor_config_Val` executable to check
    the default via:

    ```console
    $ condor_config_val JOB_DEFAULT_REQUESTMEMORY
    2048
    ```

### `requirements`

Jobs should indicate what demands they make of the target machine using
the `requirements` command.
The requirements are checked against the Machine ClassAds
(see [_ClassAds_](classads.md) for more details) for those
machines in the pool to match the job to ony those machines that
match the request, e.g. to only run a job on machines that use
Red Hat Enterprise Linux 8:

```ini
requirements = (OpSysAndVer == "RedHat8")
```

### `should_transfer_files`

See [_Manually enabling the file transfer mechanism_](file-transfer.md#should_transfer_files).

### `transfer_input_files`

See [_Input files_](file-transfer.md#transfer_input_files).

### `transfer_output_files`

See [_Output files_](file-transfer.md#transfer_output_files).

### `transfer_output_remaps`

See [_Remapping output files_](file-transfer.md#transfer_output_remaps).

### `universe`

**Ref**: <https://htcondor.readthedocs.io/en/latest/users-manual/choosing-an-htcondor-universe.html>  
**Default value**: `vanilla`

HTCondor separates jobs of different types into 'universes'.
In most situations there are only two useful universes:

**Vanilla**
: The default universe is `vanilla` which basically acts just like a standard
  shell environment on the remote machine.

**Local**
: The `local` universe exists only on the submit machine and causes jobs
  to be executed immediately, directly on that machine.
  This is very useful for small jobs that require network access to
  download data, which is then passed to other jobs that run on the
  standard pool resources in the `vanilla` universe.
