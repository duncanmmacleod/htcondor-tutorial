---
title: ClassAds
---

# ClassAds in HTCondor

**Ref:**
<https://htcondor.readthedocs.io/en/latest/man-pages/classads.html>

## What are ClassAds?

HTCondor records metadata about jobs and resources using attribute-value
pairs known as ClassAds.

The attributes are typically named using CamelCase, e.g. `Owner` or
`JobStatus`, and the values can be any of booleans, integers, reals,
strings, dictionaries, lists, of the special values `UNDEFINED` and
`ERROR`.

## Using ClassAds to inspect jobs

Understanding ClassAds, and the basics of ClassAd expressions can
significantly speed up interactions with HTCondor relating to
job status, history, and available resources.


Returning to the example from the previous page, we asked `condor_history`
for the status of our job, and got this

```console
$ condor_history 22139468
 ID     OWNER          SUBMITTED   RUN_TIME     ST COMPLETED   CMD
22139468.0   duncan.macleod  4/19 09:02   0+00:00:01 C   4/19 09:03 /bin/sh --version
```

Which tells us almost nothing.
However, now that we understand ClassAds we can ask for specific details of
running and completed jobs to get something useful.

!!! tip "Work for both `condor_q` and `condor_history`"
    The following options work when querying for both running
    jobs using `condor_q` and completed jobs using `condor_history`.

### `-long` {: #opt-long }

The `-long` option prints _everything_.

```console
$ condor_history 22139468 -match 1 -long
ImageSize_RAW = 1900
DiskProvisioned = 208550
ProcId = 0
CpusProvisioned = 1
BytesSent = 0.0
AcctGroup = undefined
JobFinishedHookDone = 1618819382
JobRunCount = 1
ExitBySignal = false
NumShadowStarts = 1
MemoryProvisioned = 2048
... <etc etc> ...
```

!!! warning "Don't use `-long` without `-match` or another constraint"
    `condor_history -long ...` will output hundreds of lines for
    each job matched, so it's best to only use that when also using
    `-match` or something else that restricts the size of the output
    (e.g. `grep`).

### `-attributes` {: #opt-attributes }

We can restrict the output from `-long` by passing the `-attributes`
option with a comma-separated list of ClassAd attribute names we care
about, e.g.

```console
$ condor_history 22139468 -match 1 -long -attributes Owner,ClusterId,ExitCode
ExitCode = 0
ClusterId = 22139468
Owner = "duncan.macleod"

```

Finally we can now see that with `ExitCode = 0` our job did indeed pass.

### `-af/-autoformat` {: #opt-autoformat }

We can simplify the attribute output a little bit, especially if we want something matchine-readable, by using the `-af/-autoformat` option with a
space-separated list of ClassAd attribute names (and removing `-long`):

```console
$ condor_history 22139468 -match 1 -af Owner ClusterId ExitCode
duncan.macleod 22139468 0
```

### `-constraint` {: #opt-constraint }

For full details, see
<https://htcondor.readthedocs.io/en/latest/man-pages/classads.html#expression-syntax>.

When querying for multiple jobs with `condor_q` or `condor_history`
you can use the `-constraint` option with one or more ClassAd expressions
to only return matching jobs.
For example, to see the 5-most recent jobs that failed after more than
an hour of running:

```console
$ condor_history \
      -match 5 \
      -constraint 'ExitCode != 0 && RemoteWallClockTime >= 3600 && JobUniverse != 7' \
      -af Owner Cmd Arguments
michael.williams /home/michael.williams/.conda/envs/o4mdc-nessai/bin/bilby_pipe_analysis undefined
michael.williams /home/michael.williams/.conda/envs/o4mdc-nessai/bin/bilby_pipe_analysis undefined
michael.williams /home/michael.williams/.conda/envs/nessai/bin/bilby_pipe_analysis undefined
geraint.pratten /home/geraint.pratten/scratch/conda_envs/gwtidal/projects/bns_injections/gaussian_population_bns_O5/adiabatic_gpop_run_O5.sh undefined
anna.puecher /home/anna.puecher/.conda/envs/imp-bns-pe-fixed-env//bin/lalinference_mpi_wrapper --checkpoint-exit-code 77 --fref 30 --approx IMRPhenomD_NRTidalv2_Lorentzian --amporder 0 --neff 1000 --nlive 1024 --tolerance 0.1 --ntemps 10 --resume --adapt-temps --progress --distance-max 400 --srate 8192.0 --seglen 100.0 --a_spin1-max 0.15 --a_spin2-max 0.15 --chirpmass-min 1.144163 --chirpmass-max 1.244163 --q-min 0.5 --comp-max 3.0 --tidal --lambda1-min 0.0 --lambda1-max 5000.0 --lambda2-min 0.0 --lambda2-max 5000.0 --pmLorentzianFree --c0_lor-min 5e-26 --c0_lor-max 1e-21 --c1_lor-min 1500 --c1_lor-max 4000 --c2_lor-min 10.0 --c2_lor-max 200.0 --L1-asd ./LIGO-P1600143-v18-CE.txt --E1-asd ./LIGO-P1600143-v18-ET_D.txt --E2-asd ./LIGO-P1600143-v18-ET_D.txt --E3-asd ./LIGO-P1600143-v18-ET_D.txt --mpirun /home/anna.puecher/.conda/envs/imp-bns-pe-fixed-env//bin/mpirun --np 4 --executable /home/anna.puecher/.conda/envs/imp-bns-pe-fixed-env//bin/lalinference_mcmc --trigtime 1126562973.000000000 --randomseed 119043728 --outfile ./lalinferencemcmc-0-E1E3L1E2-1126562973.0-17.hdf5 --E1-cache /home/anna.puecher/IMP-project/PE_runs/networks-runs/ET-CE/frames/Core03-frames/E1-NR_INJECTED_CACHE-1126562825-150.lcf --E1-channel E1:NR_INJECTED --E1-flow 30.0 --E1-fhigh 4095.99 --E1-timeslide 0 --E3-cache /home/anna.puecher/IMP-project/PE_runs/networks-runs/ET-CE/frames/Core03-frames/E3-NR_INJECTED_CACHE-1126562825-150.lcf --E3-channel E3:NR_INJECTED --E3-flow 30.0 --E3-fhigh 4095.99 --E3-timeslide 0 --L1-cache /home/anna.puecher/IMP-project/PE_runs/networks-runs/ET-CE/frames/Core03-frames/L1-NR_INJECTED_CACHE-1126562825-150.lcf --L1-channel L1:NR_INJECTED --L1-flow 30.0 --L1-fhigh 4095.99 --L1-timeslide 0 --E2-cache /home/anna.puecher/IMP-project/PE_runs/networks-runs/ET-CE/frames/Core03-frames/E2-NR_INJECTED_CACHE-1126562825-150.lcf --E2-channel E2:NR_INJECTED --E2-flow 30.0 --E2-fhigh 4095.99 --E2-timeslide 0 --psdstart 1126559684.000000000 --psdlength 3175.0 --dont-dump-extras --ifo E1 --ifo E3 --ifo L1 --ifo E2
```


## Useful ClassAds

### Job ClassAds

For a full list, see
<https://htcondor.readthedocs.io/en/latest/classad-attributes/job-classad-attributes.html>.

| Attribute | Description |
| --------- | ----------- |
| `Arguments` | The arguments of the job |
| `ClusterId` | The ID of the job |
| `Cmd` | The full path of the file being executed |
| `Environment` | The list of environment variables passed to the job |
| `ExitCode` | The exit code of the job |
| `HoldReason` | A message descrbing why a job was put on hold |
| `JobStatus` | The current status of the job |
| `LigoSearchTag` | The accounting tag you used (LIGO jobs only) |
| `Owner` | The user who submitted the job |
| `RemoteWallClockTime` | The runtime of the job |

### Machine ClassAds

Machine ClassAds can be used with `condor_status` to see available
resources, e.g:

```console
$ condor_status -constraint 'Machine == "ccs8001.hawk.supercomputingwales.ac.uk"'
Name                                            OpSys      Arch   State     Activity LoadAv Mem    ActvtyTime

slot1@ccs8001.hawk.supercomputingwales.ac.uk    LINUX      X86_64 Unclaimed Idle      0.000 15089164+14:23:30
slot1_1@ccs8001.hawk.supercomputingwales.ac.uk  LINUX      X86_64 Claimed   Busy      3.990 16000  0+00:04:53
slot1_2@ccs8001.hawk.supercomputingwales.ac.uk  LINUX      X86_64 Claimed   Busy      1.000   896  0+00:19:47
slot1_3@ccs8001.hawk.supercomputingwales.ac.uk  LINUX      X86_64 Claimed   Busy      7.980 16000  0+00:52:50
... <snip> ...
slot1_62@ccs8001.hawk.supercomputingwales.ac.uk LINUX      X86_64 Claimed   Busy      0.990   896  0+01:12:26
slot1_64@ccs8001.hawk.supercomputingwales.ac.uk LINUX      X86_64 Claimed   Busy      1.000   896  0+01:12:08

               Machines Owner Claimed Unclaimed Matched Preempting  Drain

  X86_64/LINUX       35     0      34         1       0          0      0

         Total       35     0      34         1       0          0      0
```

For a full list, see
<https://htcondor.readthedocs.io/en/latest/classad-attributes/machine-classad-attributes.html>.

| Attribute | Description |
| --------- | ----------- |
| `Machine` | The fully-qualified host name of the machine |
