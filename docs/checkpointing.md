---
title: Checkpointing
---

# Self-checkpointing applications

**Ref:**
<https://htcondor.readthedocs.io/en/latest/users-manual/self-checkpointing-applications.html>

## Fair-share usage

One of the central features of HTCondor is the fair-share usage model,
whereby HTCondor attempts to give all users on the system an equal
(or proportional) share of the resources over a period of time.
This means that if a job is running for one user, and a new user
submits a job, HTCondor may 'preempt' the running job to make way
for the new job so as to share the resource.
The means that HTCondor evicts the original job to make space, but leaves
it in the queue to restart at a later time, potentially on a different
machine.

Most IGWN HTCondor pools are configured to avoid preempting short jobs,
but will happily preempt jobs that run for more than, say, 24 hours.
On the Open Science Grid pools, the preemption may happen after only 8
hours (or less).

## Checkpointing

When a job is preempted, HTCondor doesn't know how to (or at least doesn't
attempt to) save the current state of the job, and any in-memory data,
so the progress of a job may be lost if the job is evicted.
This is where checkpointing comes in.

Checkpointing is the act of saving the state of a computation to disk,
such that it can be recovered later with minimal data loss.
HTCondor leaves the semantics of checkpointing entirely up to the user,
but does provide a system to infer that checkpointing is happening.

## Basics of a self-checkpointing HTCondor job

So support checkpointing in an HTCondor job, the application should be
(re)configured to support the following lifecycle:

1. process a chunk of data
2. save the state in a 'checkpoint' file
3. exit immediately with a specific, non-zero exit code

and the job submit file should be updated to tell HTCondor about the
new checkpointing configuration:

```ini
+SuccessCheckpointExitCode = 77
+WantFTOnCheckpoint = True
transfer_output_files = example.checkpoint
```

!!! tip "The syntax is improving in HTCondor 9.0.0"
    The syntax for checkpointing has been improved in the latest version of
    HTCondor, and so as soon as 9.0.0 is rolled out across the board, you
    can use the `checkpoint_exit_code` command (and remove
    `+WantFTONCheckpoint`)

With this configuration, your potentitally long-running computation becomes
a series of connected, shorter jobs, which fit nicely into the fair-share
usage model, but don't have to restart from scratch each time.
