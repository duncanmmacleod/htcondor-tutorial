---
title: DAGs
---

# DAGs in HTCondor

**Ref:**
<https://htcondor.readthedocs.io/en/latest/users-manual/dagman-workflows.html>

## What is a DAG?

A directed acyclic graph (DAG) is a connected series of nodes
that has no loops, typically representing one or more possible
paths through a network.

In HTC, a DAG is used to represent a workflow where each node
in the graph indicates a single job and the vertices that
connect nodes represent the interdependencies between jobs.

## How to represent a DAG with HTCondor

In HTCondor, a DAG is represented by a DAG input file
(typically `<thing>.dag`) that defines each node in the graph
and each connection to other jobs.

For example, a simple linear DAG of three jobs

```mermaid
graph LR;
    A-->B
    B-->C
```

would be described as

!!! example "`linear.dag`"
    ```ini
    JOB A A.sub
    JOB B B.sub
    JOB C C.sub
    PARENT A CHILD B
    PARENT B CHILD C
    ```


The syntax of a `JOB` entry is:

```
JOB <JobName> <SubmitFileName>
```

and the dependency syntax is

```ini
PARENT <Parent JobName> [<Parent2 JobName> ... ] CHILD <Child JobName> [<Child JobName2> ... ]
```

## Simple example

We can revive our roster parsing example from the previous sections into
an end-to-end (but fairly trivial) workflow.
Here our DAG will have only two nodes

```mermaid
graph LR;
    roster-->council
```

which we represent in our DAG file as follows:

!!! example "`council.dag`"
    ```
    JOB roster roster.sub
    JOB council council.sub
    PARENT roster CHILD council
    ```

Our two submit files are:

!!! example "Submit files"

    === "roster.sub"

        ```ini
        should_transfer_files = YES
        universe = local
        executable = /bin/curl
        arguments = "-L 'https://roster.ligo.org/roster.php?do=rostercsv&search=institution&target=31:7&order='"
        output = roster.txt
        error = roster.err
        log = roster.log
        queue
        ```

    === "council.sub"

        ```ini
        should_transfer_files = YES
        universe = vanilla
        executable = /bin/grep
        arguments = ",Y,"
        input = roster.txt
        output = council.txt
        error = council.err
        log = council.log
        queue
        ```

## Submitting a DAG

While jobs were submitted with `condor_submit`, a DAG is submitted
using the special `condor_submit_dag` executable:

```bash
condor_submit_dag council.dag
```

The output should look something like this:

```console
$ condor_submit_dag workflow.dag

-----------------------------------------------------------------------
File for submitting this DAG to HTCondor           : workflow.dag.condor.sub
Log of DAGMan debugging messages                 : workflow.dag.dagman.out
Log of HTCondor library output                     : workflow.dag.lib.out
Log of HTCondor library error messages             : workflow.dag.lib.err
Log of the life of condor_dagman itself          : workflow.dag.dagman.log

Submitting job(s).
1 job(s) submitted to cluster 22139745.
-----------------------------------------------------------------------
```

When a DAG is submitted, HTCondor submits a special job called a
`DAGMan` ('DAG Manager') that exists for the lifetime of the DAG and
acts as the 'parent' for all of the other jobs that represent the
nodes in the graph.
These DAGMan jobs run in the `scheduler` universe.
Only once the DAG is complete (not necessarily to be confused with
'successful') will the DAGMan job be removed.

!!! note "DAGMAN jobs are not accounted for"
    In IGWN we track the usage of all jobs, with the explicit
    exception of DAGMan jobs which run for a long time, but use
    almost no resources.

## Tracking a DAG

### `condor_q`

As with jobs, you can see the status of a DAG using `condor_q` by
passing the ID of the DAG:

```console
$ condor_q 22139750 -dag -nobatch


-- Schedd: cl8.supercomputingwales.ac.uk : <172.2.1.88:9618?... @ 04/19/21 12:30:41
 ID          OWNER/NODENAME      SUBMITTED     RUN_TIME ST PRI SIZE CMD
22139750.0   duncan.macleod     4/19 12:30   0+00:00:18 R  0    0.3 condor_dagman -p 0 -f -l . -Lockfile workflow.dag.loc
22139752.0    |-council         4/19 12:30   0+00:00:00 I  0    0.2 grep ,Y,

Total for query: 1 jobs; 0 completed, 0 removed, 1 idle, 0 running, 0 held, 0 suspended
Total for all users: 13363 jobs; 0 completed, 0 removed, 8865 idle, 3974 running, 524 held, 0 suspended
```

Options:

- `-dag` groups jobs by their parent DAG, rather than by ID
- `-nobatch` shows the jobs in the DAG separately,
  rather than rolling everything up into one line

### The `dagman.out` file

A more informative way to track a DAG file is to watch the contents
of the `dagman.out` file that was created when the DAG began execution.
If the DAG file was called `workflow.dag` the `dagman.out` will be called
`workflow.dag.dagman.out`.

This file contains lots of logging information that isn't very useful in
real time, but every time a job changes status (or every 10 minutes,
whichever is sooner) a progress table will be printed that looks like this:

!!! example "`dagman.out` progress table"
    ```
    04/19/21 12:04:12 Of 2 nodes total:
    04/19/21 12:04:12  Done     Pre   Queued    Post   Ready   Un-Ready   Failed
    04/19/21 12:04:12   ===     ===      ===     ===     ===        ===      ===
    04/19/21 12:04:12     0       0        0       0       1          1        0
    ```

If things are progressing well, you should see jobs flowing from right
to left (`Un-Ready` through to `Done`).
Eventually, for the above example, the `council.dag.dagman.out` file
included this:

```
04/19/21 12:30:53 Of 2 nodes total:
04/19/21 12:30:53  Done     Pre   Queued    Post   Ready   Un-Ready   Failed
04/19/21 12:30:53   ===     ===      ===     ===     ===        ===      ===
04/19/21 12:30:53     2       0        0       0       0          0        0
```

## Re-submitting a failed DAG {: #rescue }

If any node in a DAG fails, the parent DAGMan process will exit as a failure.
The `dagman.out` file will also show a non-zero total in the `Failed` column
in the progress table.

In this case a new file will be written automatically that describes which
nodes in the workflow passed or failed this is called a 'rescue' DAG, and can
look something like this:

!!! example "`workflow.dag.rescue001`"
    ```
    # Rescue DAG file, created after running
    #   the workflow.dag DAG file
    # Created 4/19/2021 13:27:18 UTC
    # Rescue DAG version: 2.0.1 (partial)
    #
    # Total number of Nodes: 2
    # Nodes premarked DONE: 0
    # Nodes that failed: 1
    #   roster,<ENDLIST>

    ```

If you immediately re-submit the same DAG file, using
`condor_submit_dag workflow.dag` exactly as above, HTCondor will automatically
detect and read the rescue file and will proceed to pick up where the last
submission stopped.
It will skip over the already-completed nodes and will rerun the nodes that
were listed as failed.

This gives the user a chance to investigate any failures, correct corruption
in input data, or bugs in scripts, and then resubmit the workflow without
having to manually figure out which jobs need to be retried, or to edit the
workflow files in any way.


## Advanced DAG usage

### `VARS`

It is common to have multiple nodes in a DAG that do the same sort of
thing, but with different input arguments, for example the same data
processing algorithm running over different data segments.
In this case it would be annoying to have to duplicate the same submit
file just to tweak the arguments for each node.

Instead, HTCondor supports variable arguments for each node in a DAG
through the `VARS` command, with the following syntax:

```
VARS [<JobName> | ALL_NODES] macroname="string" [macroname2="string2" ... ]
```

Once defined, the relevant `macroname` variables can be referred to in
the submit file to allow dynamic argument to a job for each node.

To demonstrate, let's extend our roster example above by adding a job
that prints out the names of the members in a CSV file, but we want to
run this for both the full members roster, and the council-members-only
file.
Our new workflow looks like this:

```mermaid
graph LR;
    roster-->council
    roster-->roster-members
    council-->council-members
```

We can write a new job submit file for the new task:

!!! example "`members.sub`"
    ```ini
    should_transfer_files = YES
    executable = /bin/awk
    arguments = "'BEGIN { FS = \",\" } ; { print $4,$3 }'"
    input = $(tag).txt
    output = $(tag)-members.txt
    error = $(tag)-members.err
    log = $(tag)-members.log
    queue
    ```

This uses the macro `tag` which should be defined for each node
in the DAG for this job.

We can now update our DAG to include the new jobs:

!!! example "`workflow-vars.dag`"
    ```
    JOB roster roster.sub
    JOB roster-members members.sub
    JOB council council.sub
    JOB council-members members.sub
    VARS roster-members tag="roster"
    VARS council-members tag="council"
    PARENT roster CHILD roster-members council
    PARENT council CHILD council-members
    ```

When submitted and completed, this now produces two new files
`roster-members.txt` and `council-members.txt` that include
English-standard `FirstName LastName` entries for each member of
the relevant cohort.

### `RETRY`

HTCondor can be instructed to automatically retry and failed node in a DAG
via the `RETRY` command, with the following syntax:

```
RETRY [<JobName> | ALL_NODES] <NumberOfRetries> [UNLESS-EXIT value]
```

For the above `VARS`-edited workflow, the `curl` command may fail due to
a transient network issue, so is worth retrying on failure at least once.
We can modify the workflow to do that as follows:

!!! example "`workflow-vars-retry.dag`"
    ```
    JOB roster roster.sub
    JOB roster-members members.sub
    JOB council council.sub
    JOB council-members members.sub
    VARS roster-members tag="roster"
    VARS council-members tag="council"
    PARENT roster CHILD roster-members council
    PARENT council CHILD council-members
    RETRY roster 2
    ```

The default number of retries is 0.
