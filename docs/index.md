---
title: Introduction
---

# HTCondor mini-tutorial

This site presents a short tutorial on
[HTCondor](https://htcondor.readthedocs.io/),
covering the basics of how to prepare a workflow,
submit it to a scheduler, and track its progress.

This is a heavily distilled and simplified version of a number of
other, better, HTCondor tutorials, including

- <https://htcondor.readthedocs.io/>
- <https://computing.docs.ligo.org/guide/condor/>
- <https://dcc.ligo.org/LIGO-G2100613>
