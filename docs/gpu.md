---
title: GPUs
---

# Using GPUs with HTCondor

**Ref:**
<https://htcondor.readthedocs.io/en/latest/users-manual/submitting-a-job.html#jobs-that-require-gpus>

## `request_gpus`

To request a job run on a GPU machine, simply add the `request_gpus`
command to the submit file alongside the `request_cpus` command:

```ini
request_cpus = 1
request_gpus = 1
```

## Matching containers and GPU drivers

When running a job in a container, you should take care to match
the CUDA runtime installed in the container, with the CUDA drivers
installed on the actual host.
This can be done by specifying the `CUDACapability` as a machine requirement:

```ini
requirements = CUDACapability >= 9
```
