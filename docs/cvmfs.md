---
title: CVMFS
---

# Using CVMFS with HTCondor

The [Cern-VM File System](https://cernvm.cern.ch/) (CVMFS) is a tool
for distributing files (data or software) globally, and is used heavily
in IGWN for distribution of instrument (GW strain) data and software
environments.

## CVMFS repositories as Machine requirements {: #requirements }

!!! warning "This is not ready yet"

    As of April 2021, this configuration is only valid on the distributed
    IGWN grid, and is not configured on the 'local' HTCondor pools operated
    by any of the LIGO Data Grid computing centres.

    However, it is likely that this will become standard across all IGWN
    resources shortly.

The Open Science Grid (OSG) distributed HTC 'glidein' mechanism configures
its dynamic slots to advertise the CVMFS repositories available on a machine
as boolean Machine ClassAds with the following syntax:

| Repository name | ClassAd variable |
| --------------- | ---------------- |
| `thing.domain.org` | `HAS_CVMFS_thing_domain_org` |

For example, to require that a target machine has the
`oasis.opensciencegrid.org` repository configured, you should add the
following requirement to a job submission file

```ini
requirements = HAS_CVMFS_oasis_opensciencegrid_org =?= TRUE
```


## Special case: IGWN proprietary data {: #has_ligo_frames }

There is a special exception to the above CVMFS variable convention.
To ensure that a grid job lands on a host that has the CVMFS-distributed
proprietary IGWN data available, you should use the `HAS_LIGO_FRAMES`
variable.

For example, if you are running a job that uses an IGWN Conda Distribution
environment in CVMFS and needs LIGO CVMFS data, you should use this combined
requirement:

```ini
requirements = HAS_CVMFS_oasis_opensciencegrid_org =?= TRUE && HAS_LIGO_FRAMES =?= TRUE
```
