---
title: File transfers
---

# Input and output files for HTCondor jobs

**Ref:**
<https://htcondor.readthedocs.io/en/latest/users-manual/file-transfer.html>

By default, HTCondor runs each job in a 'sandbox' with no access
to the `/home/` directory of the owner, or to other filesystems,
and doesn't transfer anything into or out of the sandbox at either
end of the job lifetime.

## Using the standard data streams {: #streams }

The standard data streams (input, output, and error) are automatically
transferred into and out of a job if specified in the submission file.

### Standard output (`output`) {: #output }

We have already seen the most common example of data input/output,
that of capturing the standard output stream (`stdout`) from a job
and recording that in a file.

The `output` file specified in a job submission file is automatically
transferred out of the job sandbox back to the submission directory.

### Standard error (`error`) {: #error }

In almost all circumstances, it is also useful to capture the
standard error stream (`stderr`) from a job - if your job fails this
will likely contain the information needed to work out why.

To capture `stderr` to a file, use the `error` command in your submit file

```
error = job.err
```

### Standard input (`input`) {: #input }

If a script reads from the standard input stream (`stdin`), you can
specify a file to read from using the `input` command.
For example, we can download the roster of our group from roster.ligo.org:

```bash
curl -L "https://roster.ligo.org/roster.php?do=rostercsv&search=institution&target=31:7&order=" > roster.txt
```

And can then design a condor job to list only the council members:

!!! example "`council.sub`"
    ```ini
    executable = /bin/grep
    arguments = ",Y,"
    input = roster.txt
    output = council.txt
    queue
    ```

In this case the `input` file `roster.txt` is automatically transferred
into the sandbox, and the output file `council.txt` is returned.
We should end up with this:

!!! example "`council.txt`"
    ```csv
    N,Y,Fairhurst,Stephen,LSC - GEO - Cardiff University,stephen.fairhurst, +44 (29) 2087 0166
    N,Y,Grote,Hartmut,LSC - GEO - Cardiff University,hartmut.grote, +44 (29208) 76460
    N,Y,Hannam,Mark,LSC - GEO - Cardiff University,mark.hannam, +44 (29) 208 74458
    N,Y,Raymond,Vivien,LSC - GEO - Cardiff University,vivien.raymond, +44 (29) 2068 8915
    N,Y,Schutz,Bernard,LSC - GEO - Cardiff University,bernard.schutz, +44 (7535) 650477
    N,Y,Sutton,Patrick,LSC - GEO - Cardiff University,patrick.sutton, +44 (029) 2087 4043
    ```

## The file transfer mechanism

The file transfer mechanism in HTCondor allows the user to explicitly
declare the file requirements of a job, rather than relying on a
shared file system to supply them.

Shared file systems are easily overloaded when large numbers of jobs
read and write from lots of files at the same time, and are very often
slow to transfer files even at the best of times.
This is especially true of a file system like NFS, which is commonly
used for user `/home` directories...

By default HTCondor will try and detect whether file transfers are
necessary, based on the `FileSystemDomain` setting of the matched
remote machine, but it is better to not rely on this and instead
explicitly enable the file transfer mechanism.

### Manually enabling the file transfer mechanism {: #should_transfer_files }

You should always enable the file transfer mechanism if your job
requires any files that aren't definitely available on the remote
machine that will run your job:

!!! tip "Always enable the file transfer mechanism"

    ```
    should_transfer_files = YES
    ```

If a job requires an input file, or writes an output file that
should be saved, these need to be declared in the job submission
file using the `transfer_input_files` and `transfer_output_files`
commands.

### Input files {: #transfer_input_files }

We can restructre the [above](#stdin) example to have `grep` use
the `roster.txt` file as an input file, instead of reading from `stdin:

!!! example "`council.sub`"
    ```ini
    executable = /bin/grep
    arguments = "',Y,' roster.txt"
    output = council.txt
    error = council.err
    queue
    ```

However, when we submit this file to the queue, it will eventually fail
because the input file wasn't transferred into the job sandbox:

```console
$ cat council.err
condor_exec.exe: roster.txt: No such file or directory
```

We need to specify the list of files to be transferred from the submission
directory using the `transfer_input_files` command:

!!! example "`council.sub`"
    ```ini
    executable = /bin/grep
    arguments = "',Y,' roster.txt"
    output = council.txt
    error = council.err
    should_transfer_files = YES
    transfer_input_files = roster.txt
    queue
    ```

Now we should get the same output as above.

### Output files {: #transfer_output_files }

If a job writes output files, they should be specified using the
`transfer_output_files` command.

!!! note "By default, everything written is transferred"
    If the file transfer mechanism is enabled, all files created
    in the job sandbox are transferred, but in the author's opinion,
    relying on this feature is asking for trouble - it's better to
    be explicit.

We can imagine a more complicated version of our council roster
example that uses Python to read the roster from a file and write
the council members to a new file:

!!! example "`council.py`"
    ```python
    #!/usr/bin/python
    from __future__ import print_function
    import csv
    with open("roster.txt", "r") as roster, open('council.txt', "w") as council:
        for row in csv.reader(roster):
            if row[1] == "Y":
                print("{} is a council member".format(row[2]))
                print(",".join(row), file=council)
    ```

We need to now specify both the input file (`roster.txt`) and the
output file (`council.txt`) to make sure everything goes smoothly:

!!! example "`council.sub`"
    ```ini
    executable = council.py
    output = council.out
    error = council.err
    should_transfer_files = YES
    transfer_input_files = roster.txt
    transfer_output_files = council.txt
    queue
    ```

In this instance we get both the standard output file:

!!! example "`council.out`"
    ```
    Fairhurst is a council member
    Grote is a council member
    Hannam is a council member
    Raymond is a council member
    Schutz is a council member
    Sutton is a council member
    ```

and the data output file as before:

!!! example "`council.txt`"
    ```
    N,Y,Fairhurst,Stephen,LSC - GEO - Cardiff University,stephen.fairhurst, +44 (29) 2087 0166
    N,Y,Grote,Hartmut,LSC - GEO - Cardiff University,hartmut.grote, +44 (29208) 76460
    N,Y,Hannam,Mark,LSC - GEO - Cardiff University,mark.hannam, +44 (29) 208 74458
    N,Y,Raymond,Vivien,LSC - GEO - Cardiff University,vivien.raymond, +44 (29) 2068 8915
    N,Y,Schutz,Bernard,LSC - GEO - Cardiff University,bernard.schutz, +44 (7535) 650477
    N,Y,Sutton,Patrick,LSC - GEO - Cardiff University,patrick.sutton, +44 (029) 2087 4043
    ```

### Transferring executables {: #transfer_executable }

When the file transfer mechanism is enabled, by default HTCondor will also
transfer the `executable` of the job to the remote machine.
This is normally what you want when the executable is a custom script
that you have written, but will not be what you want when the executable
is pre-staged on that machine, perhaps with CVMFS or when using a container.

To disable the executable transfer, use the following submit file command:

```
transfer_executable = False
```

!!! tip "Always disable executable transfers when using Conda executables"
    Most conda packages distribute executables that are compiled with
    relative-path (`rpath`) links to other shared object libraries.
    If HTCondor then transfers the executable into the sandbox of
    the job, even though the libraries might be available in their
    original location (e.g. `/cvmfs/oasis.opensciencegrid.org/...`)
    the relative links will be broken, and your job will fail.

    So, to avoid this, always use `transfer_executable = False` when
    the `executable` is a file in a conda environment
    (even if not using CVMFS).

### Extras

#### File transfers with URLs {: #urls }

HTCondor can also accept URLs as input (and output) files, and will directly
download (upload) data into the job sandbox, for example we could skip over
the use of `curl` in the above example, and just pass the original URL as the
input file:

```ini
transfer_input_files = "https://roster.ligo.org/roster.php?do=rostercsv&search=institution&target=31:7&order="
```

#### Remapping output files {: #transfer_output_remaps }

HTCondor supports transferring files out of a job, and renaming them at the
same time.
The `transfer_output_remaps` command can be used in a submit file to declare
a semicolon-separated mapping of `name = newname` declarations, e.g:

```
transfer_output_files = roster.txt
transfer_output_remaps = "roster.txt = cardiff-roster.txt"
```

This will result in a file `cardiff-roster.txt` being the final output file
that is sent back to the submit directory on the submit machine.

!!! warning "Doesn't work with directories"
    Directories cannot be renamed using `transfer_output_remaps`.

#### Preserving relative paths {: #preserve_relative_paths }

By default, HTCondor's file transfer mechanism transfers files into and out of
a flat namespace, and will ignore directories in both input and output file
paths.
For example, the following declaration

```ini
transfer_input_files = cache.txt,data/file1.txt
```

will result in both `cache.txt` and `file1.txt` being transferred directly into
the base sandbox directory, ignoring the `data/` directory for the second file.
This can lead to some confusing logic on how to refer to files in job arguments.
